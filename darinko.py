# http://web.archive.org/web/20160817103207/http://sleekxmpp.com/getting_started/muc.html
# -*- coding: utf8 -*-
import sys
import logging
import getpass
from optparse import OptionParser

if sys.version_info < (3, 0):
    reload(sys)
    sys.setdefaultencoding('utf8')
else:
    raw_input = input

import sleekxmpp







class MUCBot(sleekxmpp.ClientXMPP):

  def __init__(self, jid, password, room, nick):
    sleekxmpp.ClientXMPP.__init__(self, jid, password)

    self.room = room
    self.nick = nick


    self.add_event_handler("session_start", self.start)
    self.add_event_handler("groupchat_message", self.muc_mainloop)
    self.add_event_handler("muc::%s::got_online" % self.room,
                           self.muc_online)

    # Dictionary of key=trigger_string, value=handler_function pairs.
    # Every time when bot "gets" a message, it runs through the keys,
    # and if it finds the key in the message, it calls the handler_function
    # giving it 2 params; sender (string) and message(string).
    self.trigger_handler=dict()



  def TriggerHandler(self,trigger_string):
    def real_decorator(func):
      self.trigger_handler[trigger_string.lower()]=func
      def wrapper():
        raise ValueError("The function is empty after using the decorator")
        return
      return wrapper
    return real_decorator



  def start(self, event):
    self.get_roster()
    self.send_presence()
    self.plugin['xep_0045'].joinMUC(self.room,self.nick,wait=True)
  def muc_mainloop(self, msg):
    self.msg=msg
    
    for key in self.trigger_handler.keys():
      if key in self.msg["body"].lower():
        sender=self.msg["mucnick"]
        message_body=self.msg["body"]
        handler_function=self.trigger_handler[key]
        response=handler_function(sender, message_body)

        self.send_message(mto=msg['from'].bare,mbody="{}".format(response),mtype='groupchat')


    # In the following "block" add your custom "triggers" and "responses".

    @self.TriggerHandler("example")
    def example(sender,message):
      print(sender," sent the message.")
      print(message," -> is what he sent")
      print("I can do whatever I want with that.")

      return "hi there {}, nice to meet you! Your message has {} chars!".format(sender,len(message))
    
    @self.TriggerHandler("idem")
    def idem(sender, message):
      words = message.split(' ')
      for idx, word in enumerate(words):
        if word.lower() == 'idem':
          try:
            verb = words[idx+1].lower()
            if verb[-1] == 't' or verb[-2:] == 'ti':
              if verb[-1] == 't':
                verb_stem = verb[:-1]
              else:
                verb_stem = verb[:-2]
          except Exception as e:
            pass
      return "%s: %sli ti tako." % (sender,verb_stem)

    @self.TriggerHandler("kaj")
    def kaj(sender,message):
      import random
      izlike=["slušam","hodam","sjedim","pišam"]
      return "Ja dok {} ne čujem.".format(random.choice(izlike))

    # /block


  def muc_online(self, presence):
    if presence['muc']['nick'] != self.nick:
      pass

if __name__ == '__main__':
    # Setup the command line arguments.
    optp = OptionParser()

    # Output verbosity options.
    optp.add_option('-q', '--quiet', help='set logging to ERROR',
                    action='store_const', dest='loglevel',
                    const=logging.ERROR, default=logging.INFO)
    optp.add_option('-d', '--debug', help='set logging to DEBUG',
                    action='store_const', dest='loglevel',
                    const=logging.DEBUG, default=logging.INFO)
    optp.add_option('-v', '--verbose', help='set logging to COMM',
                    action='store_const', dest='loglevel',
                    const=5, default=logging.INFO)

    # JID and password options.
    optp.add_option("-j", "--jid", dest="jid",
                    help="JID to use")
    optp.add_option("-p", "--password", dest="password",
                    help="password to use")
    optp.add_option("-r", "--room", dest="room",
                    help="MUC room to join")
    optp.add_option("-n", "--nick", dest="nick",
                    help="MUC nickname")

    opts, args = optp.parse_args()

    # Setup logging.
    logging.basicConfig(level=opts.loglevel,
                        format='%(levelname)-8s %(message)s')

    if opts.jid is None:
        opts.jid = raw_input("Username: ")
    if opts.password is None:
        opts.password = getpass.getpass("Password: ")
    if opts.room is None:
        opts.room = raw_input("MUC room: ")
    if opts.nick is None:
        opts.nick = raw_input("MUC nickname: ")

    # Setup the MUCBot and register plugins. Note that while plugins may
    # have interdependencies, the order in which you register them does
    # not matter.
    xmpp = MUCBot(opts.jid, opts.password, opts.room, opts.nick)
    xmpp.register_plugin('xep_0030') # Service Discovery
    xmpp.register_plugin('xep_0045') # Multi-User Chat
    xmpp.register_plugin('xep_0199') # XMPP Ping

    # Connect to the XMPP server and start processing XMPP stanzas.
    if xmpp.connect():
        # If you do not have the dnspython library installed, you will need
        # to manually specify the name of the server if it does not match
        # the one in the JID. For example, to use Google Talk you would
        # need to use:
        #
        # if xmpp.connect(('talk.google.com', 5222)):
        #   ...
        xmpp.process(block=True)
        print("Done")
    else:
        print("Unable to connect.")